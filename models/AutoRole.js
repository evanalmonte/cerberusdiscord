module.exports = class AutoRole {
  constructor(id, guildId, roleId, rolename, membershipJoinDuration) {
    this.id = id;
    this.guildId = guildId;
    this.roleId = roleId;
    this.rolename = rolename;
    this.membershipJoinDuration = membershipJoinDuration;
  }
};

