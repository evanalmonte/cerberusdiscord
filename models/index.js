const fs = require("fs");
var path = require("path");

const files = fs
  .readdirSync(path.join(__dirname, './'))
  .filter((file) => !file.startsWith("index"));
for (const modelFile of files) {
  const model = require(`./${modelFile}`);
  module.exports[model.name] = model;
}
