const Enum = require("Enum")
module.exports = new Enum(["m", "h", "d", "w", "M"], "TimeUnit");