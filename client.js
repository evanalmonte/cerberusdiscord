const { CommandoClient, SyncSQLiteProvider } = require("discord.js-commando");

const path = require("path");
const { db, getAutoRoles } = require("./db");

const client = new CommandoClient({
  commandPrefix: "*",
  owner: "748533361345036368",
  disableEveryone: true,
});

client.registry
  .registerDefaultTypes()
  .registerGroups([["admin", "Admin Commands"]])
  .registerDefaultGroups()
  .registerDefaultCommands({
    commandState: false,
    unknownCommand: false,
    help: false,
    prefix: false,
    eval: false,
    ping: false,
  })
  .registerCommandsIn(path.join(__dirname, "commands"));

const provider = new SyncSQLiteProvider(db);
client.setProvider(provider).then(async () => {
  // Add a property to the client to represent the current timers that are active.
  client.cerberusTimers = new Map();
  client.guilds.cache.forEach(async (guild) => {
    // Put Cerberus in the OFF status.
    client.provider.set(guild.id, "status", "OFF");
    const guildTimerMap = new Map();
    client.cerberusTimers.set(guild.id, guildTimerMap);
    getAutoRoles(guild.id).then((autoRoleMap) => {
      autoRoleMap.forEach((role) => {
        guildTimerMap.set(role.roleId, new Map());
      });
    });
    // Set Ceberus' nickname to a useful name representing how to call commands
    if (guild.commandPrefix === null) {
      guild.me.setNickname(
        `${client.commandPrefix} || @${client.user.username}`
      );
    } else {
      guild.me.setNickname(
        `${guild.commandPrefix} || @${client.user.username}`
      );
    }
    // Set the default notification channel to be used by Ceberus for information messages
    const notificationChannelId = guild.settings.get("notification_channel_id");
    if (!notificationChannelId) {
      guild.settings.set("notification_channel_id", guild.systemChannelID);
      client.notificationChannel = guild.systemChannel;
    } else {
      const notificationChannel = guild.channels.cache.get(
        notificationChannelId
      );
      if (!notificationChannel) {
        guild.settings.set("notification_channel_id", guild.systemChannelID);
      }
    }
  });
});

/**
 * Retrieves the notification channel for the guild provided
 * @param {The guild for which the notification channel will be retrieved} guild
 * @returns The notification channel for the specified guild if it can be resolved, otherwise undefined
 */
client.getNotificationsChannel = async (guild) => {
  const notificationsChannelId = guild.settings.get("notification_channel_id");
  try {
    const notificationChannel = await guild.channels.resolve(
      notificationsChannelId
    );
    return notificationChannel;
  } catch (error) {
    console.log(error);
  }
};

process.on("exit", () => db.close());

module.exports = {
  client,
};
