const moment = require("moment");

const { compareRolesByDurationDesc } = require("../util");
const { getAutoRoles } = require("../db");

async function removePreviousAutoRole(member, timerMap, guildAutoRolesMap) {
  var previousAutoRoles = member.roles.cache
    .array()
    .filter((role) => guildAutoRolesMap.has(role.id));
  if (
    previousAutoRoles.length !== 0 &&
    member.guild.roles.cache.has(previousAutoRoles[0].id)
  ) {
    const previousRoleTimers = timerMap.get(previousAutoRoles[0].id);
    previousRoleTimers.delete(member.user.id);
    await member.roles.remove(previousAutoRoles[0].id);
  }
}
async function giveMemberNextRole(client, member, removePrevious) {
  if (client.provider.get(member.guild, "status") === "OFF") {
    return;
  }
  if (!member || member.user.bot || member.deleted) {
    return;
  }
  const guildAutoRolesMap = await getAutoRoles(member.guild.id);
  if (guildAutoRolesMap.size === 0) {
    return;
  }
  const memberJoinedAtMoment = moment(member.joinedTimestamp);
  const currentTimestamp = moment();
  const memberJoinDurationMoment = moment.duration(
    currentTimestamp.diff(memberJoinedAtMoment)
  );
  const memberJoinDuration = memberJoinDurationMoment.asMilliseconds();
  const timerMap = client.cerberusTimers.get(member.guild.id);
  const guildAutoRolesArray = Array.from(guildAutoRolesMap.values()).sort(
    compareRolesByDurationDesc
  );
  if (removePrevious) {
    removePreviousAutoRole(member, timerMap, guildAutoRolesMap);
  }
  var currentRoleIndex = guildAutoRolesArray.findIndex((role) => {
    return (
      memberJoinDuration >
      moment
        .duration(
          role.membershipJoinDuration.quantity,
          role.membershipJoinDuration.timeUnit
        )
        .asMilliseconds()
    );
  });
  var nextRoleIndex = currentRoleIndex - 1;
  if (currentRoleIndex >= 0) {
    const roleToAdd = guildAutoRolesArray[currentRoleIndex];
    await member.roles.add(roleToAdd.roleId);
    const currentRoleTimers = timerMap.get(roleToAdd.roleId);
    currentRoleTimers.delete(member.user.id);
    if (guildAutoRolesArray[0].roleId === roleToAdd.roleId) {
      return;
    }
  }
  if (nextRoleIndex < 0) {
    nextRoleIndex = guildAutoRolesArray.length - 1;
  }
  const requireRoleJoinDuration = moment
    .duration(
      guildAutoRolesArray[nextRoleIndex].membershipJoinDuration.quantity,
      guildAutoRolesArray[nextRoleIndex].membershipJoinDuration.timeUnit
    )
    .asMilliseconds();
  const timer = setTimeout(
    giveMemberNextRole,
    requireRoleJoinDuration - memberJoinDuration,
    client,
    member,
    true
  );
  const nextRoleTimers = timerMap.get(
    guildAutoRolesArray[nextRoleIndex].roleId
  );
  nextRoleTimers.set(member.id, { timer: timer, member: member });
}

module.exports = {
  giveMemberNextRole,
};
