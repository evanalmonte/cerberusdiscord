const Discord = require("discord.js");

const { removeAutoRoleById, findAutoRoleById } = require("../db");
const { client } = require("../client");

module.exports = {
  deleteRole: async (role) => {
    const autoRole = await findAutoRoleById(role.guild.id, role.id);
    if (!autoRole) {
      return;
    }
    removeAutoRoleById(role.guild.id, role.id);
    const timerMap = this.client.cerberusTimers.get(message.guild.id);
    timerMap.delete(addedRole.roleId);
    const infoMessageEmbed = new Discord.MessageEmbed().setTitle(
      "Role Removed ❗❗"
    )
      .setDescription(`A role used by Cerberus was deleted from the server. It will no longer be auto assigned. Role removed:\n
          • ${autoRole.rolename} ➞ ${autoRole.membershipJoinDuration.quantity}${autoRole.membershipJoinDuration.timeUnit}`);
    client.getNotificationsChannel(role.guild).then((channel) => {
      channel.send(infoMessageEmbed);
    });
  },
};
