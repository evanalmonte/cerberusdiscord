const moment = require("moment");
const Discord = require("discord.js");

module.exports = {
  compareRolesByDurationAsc: (role1, role2) => {
    const role1DurationAsMilli = moment
      .duration(
        role1.membershipJoinDuration.quantity,
        role1.membershipJoinDuration.timeUnit
      )
      .asMilliseconds();
    const role2DurationAsMilli = moment
      .duration(
        role2.membershipJoinDuration.quantity,
        role2.membershipJoinDuration.timeUnit
      )
      .asMilliseconds();
    return role1DurationAsMilli - role2DurationAsMilli;
  },
  compareRolesByDurationDesc: (role1, role2) => {
    const role1DurationAsMilli = moment
      .duration(
        role1.membershipJoinDuration.quantity,
        role1.membershipJoinDuration.timeUnit
      )
      .asMilliseconds();
    const role2DurationAsMilli = moment
      .duration(
        role2.membershipJoinDuration.quantity,
        role2.membershipJoinDuration.timeUnit
      )
      .asMilliseconds();
    return role2DurationAsMilli - role1DurationAsMilli;
  },

  checkMemberPermissions: (member) => {
    const requiredPermissions = [
      Discord.Permissions.FLAGS.MANAGE_ROLES,
      Discord.Permissions.FLAGS.MANAGE_GUILD,
    ];
    
    for (permission of requiredPermissions) {
      if (
        member.hasPermission(permission, {
          checkAdmin: true,
          checkOwner: true,
        })
      ) {
        return true;
      }
    }
    return false;
  },
};
