module.exports = class InvalidRoleError extends Error {
    constructor(roleID, message) {
        super(message);
        this.roleID = roleID;
    }
}