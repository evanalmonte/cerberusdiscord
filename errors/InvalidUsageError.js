module.exports = class InvalidUsageError extends Error {
    constructor(usage, message) {
        super(message);
        this.usage = usage;
    }
}