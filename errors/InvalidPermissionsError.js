module.exports = class InvalidPermissionsError extends Error {
    constructor(pemissions, memberId, message) {
        super("User does not have privelage to use the command");
        this.pemissions = pemissions;
        this.memberId = memberId
    }
}