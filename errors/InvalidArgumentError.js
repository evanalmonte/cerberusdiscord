module.exports = class InvalidArgumentError extends Error {
    constructor(arg, message) {
        super(message);
        this.arg = arg;
    }
}