const fs = require("fs");
const files = fs.readdirSync("./errors").filter((file) => file.endsWith("Error.js"));
for (const error_file of files) {
  const error = require(`./${error_file}`);
  module.exports[error.name] = error
}