const Database = require("better-sqlite3");
const db = new Database("cerberus.db");
const { AutoRole } = require("./models");
const cloneDeep = require("lodash.clonedeep");
db.prepare(
  `CREATE TABLE IF NOT EXISTS auto_role ( 
    id INTEGER PRIMARY KEY AUTOINCREMENT, 
    guild_id VARCHAR(18) NOT NULL,
    role_id VARCHAR(18) UNIQUE NOT NULL,
    rolename TEXT NOT NULL,
    membership_join_duration INTEGER NOT NULL,
    time_unit CHAR NOT NULL)`
).run();

const autoRoleCache = new Map();

module.exports = {
  db,
  /**
   *
   * @param {The guild id to retrieve the auto roles for} guildId
   * @returns A map containing all roles being used by the autroll feature
   */
  getAutoRoles: async function (guildId) {
    if (autoRoleCache.has(guildId)) {
      return cloneDeep(autoRoleCache.get(guildId));
    }
    const stmt = db.prepare(
      "SELECT id, guild_id, role_id, rolename, membership_join_duration, time_unit FROM auto_role WHERE guild_id = ?"
    );
    const result = stmt.all(guildId);
    const cachedRoles = new Map();
    result.forEach((row) => {
      cachedRoles.set(
        row.role_id,
        new AutoRole(row.id, row.guild_id, row.role_id, row.rolename, {
          quantity: row.membership_join_duration,
          timeUnit: row.time_unit,
        })
      );
    });
    autoRoleCache.set(guildId, cachedRoles);
    return cloneDeep(cachedRoles);
  },
  /**
   *
   * @param {The guild in which to remove the autorole from} guildId
   * @param {The id of the role to remove} roleId
   * @returns undefined
   */
  removeAutoRoleById: async function (guildId, roleId) {
    const stmt = db.prepare(
      "DELETE from auto_role WHERE guild_id = ? AND role_id = ?"
    );
    const info = stmt.run(guildId, roleId);
    if (info.changes !== 1) {
      return;
    }
    const cachedRoles = autoRoleCache.get(guildId);
    if (!cachedRoles) {
      return;
    }
    cachedRoles.delete(roleId);
  },
  /**
   *
   * @param {The role to update} autoRole
   * @returns undefined
   */
  updateAutoRole: async function (autoRole) {
    const stmt = db.prepare(
      "UPDATE auto_role SET role_id = ? AND membership_join_duration = ? AND time_unit = ? WHERE guild_id = ? AND role_id = ?"
    );
    stmt.run(
      autoRole.roleId,
      autoRole.membershipJoinDuration.quantity,
      autorole.membershipJoinDuration.timeUnit,
      autoRole.guildId,
      autoRole.roleId
    );
    const cachedRoles = autoRoleCache.get(autoRole.guildId);
    if (!cachedRoles) {
      return;
    }
    const role = cachedRoles.get(autoRole.roleId);
    role.roleId = autoRole.roleId;
    role.membershipJoinDuration.quantity =
      autoRole.membershipJoinDuration.quantity;
    role.membershipJoinDuration.timeUnit =
      autoRole.membershipJoinDuration.timeUnit;
    role.guildId = autoRole.guildId;
  },
  /**
   * 
   * @param {The autorole to add} autoRole 
   * @returns The autorole added
   */
  addAutoRole: async function (autoRole) {
    var stmt = db.prepare(
      "INSERT INTO auto_role (guild_id, role_id, rolename, membership_join_duration, time_unit) VALUES (?, ?, ?, ?, ?) RETURNING id, guild_id, role_id, rolename, membership_join_duration, time_unit"
    );
    const changes = stmt.run(
      autoRole.guildId,
      autoRole.roleId,
      autoRole.rolename,
      autoRole.membershipJoinDuration.quantity,
      autoRole.membershipJoinDuration.timeUnit
    );
    stmt = db.prepare(
      "SELECT id, guild_id, role_id, rolename, membership_join_duration, time_unit FROM auto_role WHERE id = ?"
    );
    const row = stmt.get(changes.lastInsertRowid);

    const cachedRoles = autoRoleCache.get(autoRole.guildId);
    const roleCreated = new AutoRole(
      row.id,
      row.guild_id,
      row.role_id,
      row.rolename,
      {
        quantity: row.membership_join_duration,
        timeUnit: row.time_unit,
      }
    );
    if (cachedRoles) {
      cachedRoles.set(row.role_id, cloneDeep(roleCreated));
    }
    return roleCreated;
  },

  /**
   * Finds an autorole by its role id and guild id.
   * @param {The id of the guild id to find the auto role for} guildId 
   * @param {The of the role to find} roleId 
   * @returns The role found or null if the role has not been added.
   */
  findAutoRoleById: async function (guildId, roleId) {
    const cachedRoles = autoRoleCache.get(guildId);
    if (cachedRoles) {
      return cloneDeep(cachedRoles.get(roleId));
    }
    const stmt = db.prepare(
      "SELECT id, role_id, rolename, membership_join_duration, time_unit FROM auto_role WHERE guild_id = ? AND role_id = ?"
    );
    const rows = stmt.all(guildId, roleId);
    if (rows.length === 0) {
      return null;
    }
    return new AutoRole(
      rows[0].id,
      rows[0].guild_id,
      rows[0].role_id,
      rows[0].rolename,
      {
        quantity: rows[0].membership_join_duration,
        timeUnit: rows[0].time_unit,
      }
    );
  },
};
