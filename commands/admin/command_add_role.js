const Discord = require("discord.js");
const moment = require("moment");

const { Command } = require("discord.js-commando");
const { InvalidArgumentError } = require("../../errors");
const { getAutoRoles, addAutoRole } = require("../../db");
const { AutoRole } = require("../../models");
const {
  compareRolesByDurationAsc,
  checkMemberPermissions,
} = require("../../util");
const {
  giveMemberNextRole,
} = require("../../listeners/GuildMemberAddListener");

module.exports = class AddRoleCommand extends Command {
  constructor(client) {
    super(client, {
      name: "addrole",
      group: "admin",
      memberName: "add_role",
      description: `Adds a role to be given to users once they've been a member of the server for the specified duration.`,
      examples: ["addrole 763219180248891407 3 h"],
      argsPromptLimit: 0,
      guildOnly: true,
      args: [
        {
          key: "role_id",
          prompt: "The role id to be added",
          type: "string",
        },
        {
          key: "membership_join_duration",
          prompt:
            "The minimum amount of time a user must be in the server before being assigned this role, this is written with a number follwed by a time unit. The following units are valid: 's', 'm', 'h', 'd', 'w', 'M' where s represents seconds, m represents minutes, h represents hours, d represents days, w represents weeks and M represents months. For example, 12m represents 12 minutes, 5M represents 5 months.",
          type: "string",
          validate: (arg) => {
            return /^([0-9][0-9]*)([smhdwM])$/.test(arg);
          },
        },
      ],
    });
  }

  async run(message, args) {
    if (!checkMemberPermissions(message.member)) {
      throw new InvalidPermissionsError(
        Discord.Permissions.FLAGS.MANAGE_ROLES |
          Discord.Permissions.FLAGS.MANAGE_GUILD,
        message.author.id
      );
    }
    const durationParts = args.membership_join_duration.match(
      /([0-9][0-9]*)([smhdwM])/
    );
    const argsJoinDuration = {
      quantity: durationParts[1],
      timeUnit: durationParts[2],
    };
    const roleToAdd = message.guild.roles.cache.get(args.role_id);
    if (!roleToAdd) {
      throw new InvalidArgumentError(
        args.role_id,
        "The role_id entered is not a valid role"
      );
    }
    const autoRoleList = Array.from(
      (await getAutoRoles(message.guild.id)).values()
    );
    for (const autoRole of autoRoleList) {
      if (autoRole.roleId === args.role_id) {
        throw new InvalidArgumentError(
          args.role_id,
          "A role with that id is already being used by Cerberus"
        );
      } else if (
        moment
          .duration(
            autoRole.membershipJoinDuration.quantity,
            autoRole.membershipJoinDuration.timeUnit
          )
          .asMilliseconds() ===
        moment
          .duration(argsJoinDuration.quantity, argsJoinDuration.timeUnit)
          .asMilliseconds()
      ) {
        throw new InvalidArgumentError(
          `${argsJoinDuration.quantity}${argsJoinDuration.timeUnit}`,
          "A role with that time duration is already being used by Cerberus, remove it first to add a role with the same duration."
        );
      }
    }
    const roleAdded = await addAutoRole(
      new AutoRole(-1, message.guild.id, args.role_id, roleToAdd.name, {
        quantity: argsJoinDuration.quantity,
        timeUnit: argsJoinDuration.timeUnit,
      })
    );
    this.#updateRoleTimers(message.guild.id, roleAdded);
    const successEmbed = new Discord.MessageEmbed()
      .setTitle("Role Added  ✅")
      .setDescription(
        `Successfully added the role \`${roleAdded.rolename}\` with a duration of ${argsJoinDuration.quantity}${argsJoinDuration.timeUnit}`
      );
    message.channel.send(successEmbed);
  }

  async #updateRoleTimers(guildId, roleAdded) {
    const autoRoleList = Array.from((await getAutoRoles(guildId)).values());
    const timerMap = this.client.cerberusTimers.get(guildId);
    timerMap.set(roleAdded.roleId, new Map());
    autoRoleList.sort(compareRolesByDurationAsc);
    const nextRoleIndex = autoRoleList.findIndex((role) => {
      return (
        moment
          .duration(
            role.membershipJoinDuration.quantity,
            role.membershipJoinDuration.timeUnit
          )
          .asMilliseconds() >
        moment
          .duration(
            roleAdded.membershipJoinDuration.quantity,
            roleAdded.membershipJoinDuration.timeUnit
          )
          .asMilliseconds()
      );
    });
    if (nextRoleIndex != -1) {
      const roleTimerMap = timerMap.get(autoRoleList[nextRoleIndex].roleId);
      for (const timerInfo of roleTimerMap.values()) {
        clearTimeout(timerInfo.timer);
        roleTimerMap.delete(timerInfo.member.id);
        giveMemberNextRole(this.client, timerInfo.member, false);
      }
    }
  }
  onError(err, message, args) {
    /* Intentionally left blank to override implementation*/
  }
};
