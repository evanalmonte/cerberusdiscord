const Discord = require("discord.js");
const { Command } = require("discord.js-commando");

const { InvalidArgumentError } = require("../../errors");
const { checkMemberPermissions } = require("../../util");

module.exports = class HelpCommand extends Command {
  constructor(client) {
    super(client, {
      name: "help",
      group: "admin",
      memberName: "help",
      description:
        "Displays a list of available commands or help for a specific command",
      examples: ["help", "help <command_name>"],
      args: [
        {
          key: "command_name",
          prompt: "The name of the command",
          default: "",
          type: "string",
        },
      ],
    });
  }

  run(message, args) {
    if (!checkMemberPermissions(message.member)) {
      throw new InvalidPermissionsError(
        Discord.Permissions.FLAGS.MANAGE_ROLES |
          Discord.Permissions.FLAGS.MANAGE_GUILD,
        message.author.id
      );
    }
    if (args.command_name === "") {
      const helpEmbed = new Discord.MessageEmbed()
        .setColor("#ffa500")
        .setDescription(
          `Here's a list of available commands\n
            To get more information about a speciic command, use: \`${this.client.commandPrefix}help [command_name]\``
        );
      var commands = Array.from(this.client.registry.commands.values());
      if (message.channel.type === "dm") {
        commands = commands.filter((command) => !command.guildOnly);
      }
      const helpMessage = [];
      for (const command of commands) {
        helpMessage.push(`**${command.name}**\n`);
        helpMessage.push(command.description, "\n\n");
      }
      helpEmbed.addField("**Cerberus Commands**", helpMessage.join(""), false);
      message.author.createDM().then((channel) => {
        channel.send(helpEmbed);
      });
      return;
    }
    const command = this.client.registry.commands.get(args.command_name);
    if (!command) {
      throw new InvalidArgumentError(
        args.command,
        `Can't fetch help documentation for ${args.command_name}, please double check the spelling`
      );
    }
    const helpEmbed = new Discord.MessageEmbed().setColor("#ffa500");
    helpEmbed.setDescription(
      `Here are some usage examples and a description for the command \`${command.name}\`\n
          Usage legend: <optional> | (required)`
    );
    helpEmbed.addField(
      "Usage:",
      command.usage("[command_name]", undefined, null)
    );
    helpEmbed.addField("Description:", command.description);
    const argumentHelp = [];
    command.argsCollector.args.forEach((arg) => {
      argumentHelp.push(`• *${arg.label}*`, " : ", arg.prompt, "\n\n");
    });
    helpEmbed.addField("Arguments:", argumentHelp.join(""));
    message.author.createDM().then((channel) => {
      channel.send(helpEmbed);
    });
  }

  onError(err, message, args) {
    /* Intentionally left blank */
  }

  onBlock(message) {
    /* Intentionally left blank */
  }
};
