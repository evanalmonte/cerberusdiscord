const Discord = require("discord.js");
const { Command } = require("discord.js-commando");

const { InvalidArgumentError } = require("../../errors");
const { findAutoRoleById, removeAutoRoleById } = require("../../db");
const {
  giveMemberNextRole,
} = require("../../listeners/GuildMemberAddListener");
const { checkMemberPermissions } = require("../../util");

module.exports = class RemoveRoleCommand extends Command {
  constructor(client) {
    super(client, {
      name: "removerole",
      group: "admin",
      memberName: "remove_role",
      description: `Removes the role specified. Members will no longer be assigned this role.`,
      examples: ["remove_role 763219180248891407"],
      argsPromptLimit: 0,
      guildOnly: true,
      args: [
        {
          key: "role_id",
          prompt: "The role id to be added",
          type: "string",
        },
      ],
    });
  }

  async run(message, args) {
    if (!checkMemberPermissions(message.member)) {
      throw new InvalidPermissionsError(
        Discord.Permissions.FLAGS.MANAGE_ROLES |
          Discord.Permissions.FLAGS.MANAGE_GUILD,
        message.author.id
      );
    }
    const autoRoleToRemove = await findAutoRoleById(
      message.guild.id,
      args.role_id
    );
    if (!autoRoleToRemove) {
      throw new InvalidArgumentError(
        args.role_id,
        "The role_id entered is not one of the roles used by Cerberus. To add the role, use the addrole command"
      );
    }
    await removeAutoRoleById(message.guild.id, autoRoleToRemove.roleId);
    this.#updateRoleTimers(message.guild.id, autoRoleToRemove);
    const successEmbed = new Discord.MessageEmbed()
      .setTitle("Role Removed  ✅")
      .setDescription(
        `Successfully removed the role \`${autoRoleToRemove.rolename}\` with a duration of ${autoRoleToRemove.membershipJoinDuration.quantity}${autoRoleToRemove.membershipJoinDuration.timeUnit}`
      );
    this.client
      .getNotificationsChannel(message.guild)
      .then((channel) => channel.send(successEmbed));
  }

  async #updateRoleTimers(guildId, autoRoleToRemove) {
    const timerMap = this.client.cerberusTimers.get(guildId);
    const removedAutoRoleTimerMap = timerMap.get(autoRoleToRemove.roleId);
    for (const timerToRemoveInfo of removedAutoRoleTimerMap.values()) {
      clearTimeout(timerToRemoveInfo.timer);
      giveMemberNextRole(this.client, timerToRemoveInfo.member, false);
    }
    timerMap.delete(autoRoleToRemove.roleId);
  }

  onError(err, message, args) {
    /* Intentionally left blank to override implementation*/
  }
};
