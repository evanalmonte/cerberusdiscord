const Discord = require("discord.js");
const { Command } = require("discord.js-commando");

const { checkMemberPermissions } = require("../../util");

module.exports = class PrefixCommand extends Command {
  constructor(client) {
    super(client, {
      name: "prefix",
      group: "admin",
      memberName: "prefix",
      description: `Changes the prefix that Cerberus listens for. The prefix can be ${process.env.MAX_PREFIX_LENGTH} characters max`,
      argsPromptLimit: 0,
      guildOnly: true,
      examples: ["prefix ?"],
      args: [
        {
          key: "new_prefix",
          prompt: "Enter The new prefix to be used",
          type: "string",
          validate: (arg) => {
            return (
              typeof arg === "string" &&
              !/\s/g.test(arg) &&
              arg.length <= process.env.MAX_PREFIX_LENGTH
            );
          },
        },
      ],
    });
  }
  run(message, args) {
    if (!checkMemberPermissions(message.member)) {
      throw new InvalidPermissionsError(
        Discord.Permissions.FLAGS.MANAGE_ROLES |
          Discord.Permissions.FLAGS.MANAGE_GUILD,
        message.author.id
      );
    }
    message.guild.commandPrefix = args.new_prefix;
    message.guild.me.setNickname(
      `${args.new_prefix} || @${this.client.user.username}`
    );
    const successEmbed = new Discord.MessageEmbed()
      .setTitle("Prefix Changed!  ✅")
      .setDescription(
        `The new prefix for Cerberus is now \`${args.new_prefix}\``
      );
    this.client.getNotificationsChannel(message.guild).then((channel) => {
      channel.send(successEmbed);
    });
  }

  onError(err, message, args) {
    /* Intentionally left blank to override implementation*/
  }
};
