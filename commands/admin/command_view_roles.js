const Discord = require("discord.js");
const { Command } = require("discord.js-commando");

const { getAutoRoles } = require("../../db");
const { InvalidRoleError, InvalidPermissionsError } = require("../../errors");
const {
  compareRolesByDurationAsc,
  checkMemberPermissions,
} = require("../../util");

module.exports = class ViewRolesCommand extends Command {
  constructor(client) {
    super(client, {
      name: "viewroles",
      group: "admin",
      memberName: "viewroles",
      guildOnly: true,
      description: `View a list of the current roles auto assigned by Cerberus`,
      examples: ["viewroles"],
    });
  }
  async run(message) {
    if (!checkMemberPermissions(message.member)) {
      throw new InvalidPermissionsError(
        Discord.Permissions.FLAGS.MANAGE_ROLES |
          Discord.Permissions.FLAGS.MANAGE_GUILD,
        message.author.id
      );
    }
    const autoRoleList = Array.from(
      (await getAutoRoles(message.guild.id)).values()
    );
    const responseEmbed = new Discord.MessageEmbed().setColor("#ffa500");
    if (autoRoleList.size === 0) {
      responseEmbed
        .setTitle("Well Then...")
        .setDescription("There are currently no roles being used by Cerberus");
      this.client.notificationChannel.send(responseEmbed);
      return;
    }
    const descriptionMessage = [
      "Here is a list of the current roles auto assigned by Cerberus:\n\n",
    ];
    autoRoleList.sort(compareRolesByDurationAsc);
    autoRoleList.forEach((autoRole) => {
      const guildRole = message.guild.roles.cache.get(autoRole.roleId);
      if (!guildRole) {
        throw new InvalidRoleError(
          autoRole.roleId,
          `The role with id: ${autoRole.roleId} is not a valid role in the server anymore. Remove it using the command \`${message.guild.commandPrefix}removerole ${autoRole.roleId}\``
        );
      }
      descriptionMessage.push(
        `• ${autoRole.rolename} ➞ ${autoRole.membershipJoinDuration.quantity}${autoRole.membershipJoinDuration.timeUnit}\n\n`
      );
    });
    responseEmbed.setDescription(descriptionMessage.join(""));
    this.client.getNotificationsChannel(message.guild).then((channel) => {
      channel.send(responseEmbed);
    });
  }

  onError(err, message, args) { /* Intentionally left blank to override implementation*/ }
};
