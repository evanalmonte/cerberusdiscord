const Discord = require("discord.js");
const { Command } = require("discord.js-commando");

const { checkMemberPermissions } = require("../../util");

module.exports = class StatusCommand extends Command {
  constructor(client) {
    super(client, {
      name: "status",
      group: "admin",
      memberName: "status",
      guildOnly: true,
      description: "Check Ceberus' current status.",
      examples: ["status"],
    });
  }
  async run(message, args) {
    if (!checkMemberPermissions(message.member)) {
      throw new InvalidPermissionsError(
        Discord.Permissions.FLAGS.MANAGE_ROLES |
          Discord.Permissions.FLAGS.MANAGE_GUILD,
        message.author.id
      );
    }
    const status = message.guild.settings.get("status");
    const reply = new Discord.MessageEmbed().setTitle(
      `Cerberus Status: ${status}  ${status === "OFF" ? "⬇️" : "⬆️"}`
    );
    if (status === "OFF") {
      reply.setDescription(
        "Cerberus is currently off and will not give new member roles"
      );
    } else {
      reply.setDescription("Cerberus is currently on");
    }
    this.client.getNotificationsChannel(message.guild).then((channel) => {
      channel.send(reply);
    });
  }

  onError(err, message, args) {
    /* Intentionally left blank to override implementation */
  }
};
