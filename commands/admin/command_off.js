const Discord = require("discord.js");
const { Command } = require("discord.js-commando");

const { checkMemberPermissions } = require("../../util");

module.exports = class OnCommand extends Command {
  constructor(client) {
    super(client, {
      name: "off",
      group: "admin",
      memberName: "off",
      guildOnly: true,
      description:
        "Turns off Cerberus' autorole feature, new users will not automatically be given roles",
      examples: ["on"],
    });
  }
  async run(message, args) {
    if (!checkMemberPermissions(message.member)) {
      throw new InvalidPermissionsError(
        Discord.Permissions.FLAGS.MANAGE_ROLES |
          Discord.Permissions.FLAGS.MANAGE_GUILD,
        message.author.id
      );
    }
    message.guild.settings.set("status", "OFF");
    const successEmbed = new Discord.MessageEmbed()
      .setTitle("Cerberus Status Changed: OFF  ✅")
      .setDescription("Cerberus' autorole feature is now off");
    this.client.getNotificationsChannel(message.guild).then((channel) => {
      channel.send(successEmbed);
    });
  }

  onError(err, message, args) {
    /* Intentionally left blank to override implementation*/
  }
};
