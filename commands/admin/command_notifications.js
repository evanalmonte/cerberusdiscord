const Discord = require("discord.js");
const { Command } = require("discord.js-commando");

const { InvalidUsageError, InvalidArgumentError } = require("../../errors");
const { checkMemberPermissions } = require("../../util");

module.exports = class NotificationsCommand extends Command {
  constructor(client) {
    super(client, {
      name: "notifications",
      group: "admin",
      memberName: "notifications",
      description: `Changes the notification that Cerberus uses for error / info messages.`,
      argsPromptLimit: 0,
      guildOnly: true,
      examples: ["%notifications <#763219180248891411>"],
      args: [
        {
          key: "channel_mention",
          prompt:
            "A mention of the channel to be used for future notifications",
          type: "string",
          default: "",
        },
      ],
    });
  }
  run(message, args) {
    if (!checkMemberPermissions(message.member)) {
      throw new InvalidPermissionsError(
        Discord.Permissions.FLAGS.MANAGE_ROLES |
          Discord.Permissions.FLAGS.MANAGE_GUILD,
        message.author.id
      );
    }
    if (args.channel_mention === "") {
      throw new InvalidUsageError(
        this.usage("<channel_mention>", message.guild.commandPrefix),
        "No channel mention was provided."
      );
    }
    const mentionedChannels = message.mentions.channels.array();
    if (mentionedChannels.length === 0) {
      throw new InvalidArgumentError(
        args.channel_mention,
        "The channel mentioned does not exist. Please use a valid channel"
      );
    } else if (message.length > 1) {
      throw new InvalidUsageError(
        this.usage("<channel_mention>", message.guild.commandPrefix),
        "More than 1 channel was provided."
      );
    }
    this.client.notificationChannel = mentionedChannels[0];
    message.guild.settings.set(
      "notification_channel_id",
      this.client.notificationChannel.id
    );
    const successEmbed = new Discord.MessageEmbed()
      .setTitle("Notifcation Channel Changed!  ✅")
      .setDescription(
        `The new notification channel for Cerberus is now <#${this.client.notificationChannel.id}>`
      );
    this.client.notificationChannel.send(successEmbed);
  }
  onError(err, message, args) {
    /* Intentionally left blank to override implementation*/
  }

  onBlock(message, reason) {
    /* Intentionally left blank to override implementation*/
  }
};
