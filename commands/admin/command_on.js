const Discord = require("discord.js");
const { Command } = require("discord.js-commando");

const { checkMemberPermissions } = require("../../util");

module.exports = class OnCommand extends Command {
  constructor(client) {
    super(client, {
      name: "on",
      group: "admin",
      memberName: "on",
      guildOnly: true,
      description:
        "Turns on Cerberus' autorole feature, new users will now automatically be given roles as they join the server",
      examples: ["on"],
    });
  }
  async run(message, args) {
    if (!checkMemberPermissions(message.member)) {
      throw new InvalidPermissionsError(
        Discord.Permissions.FLAGS.MANAGE_ROLES |
          Discord.Permissions.FLAGS.MANAGE_GUILD,
        message.author.id
      );
    }
    message.guild.settings.set("status", "ON");
    const successEmbed = new Discord.MessageEmbed()
      .setTitle("Cerberus Status Changed: ON  ✅")
      .setDescription("Cerberus' autorole feature is now on");
    this.client.getNotificationsChannel(message.guild).then((channel) => {
      channel.send(successEmbed);
    });
  }

  onError(err, message, args) {
    /* Intentionally left blank to override implementation */
  }
};
