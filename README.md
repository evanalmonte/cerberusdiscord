# Cerberus
## _Guarding your server with all 3 heads_

![Ceberus](https://www.flaticon.com/svg/vstatic/svg/2332/2332568.svg?token=exp=1618262462~hmac=b21b1b8a3e3a628b013b8ad8cd4c6866)
## Description
Cerberus is a discord bot which automatically assigns roles to new users based on their membership age. As time progress, users will be given the oldest role available to them.
## Features

###  Add Role 
Adds a role to Cerberus' list of roles which will be given to new users after the specified duration. The role must be a valid role that exists in the server. Two roles **cannot** have the same duration.

### Remove Role
Removes a role from Cerberus' list of tracked roles. The role must be a role that is currently being assigned by Cerberus.

### View Roles
View all roles that Cerberus' is currently using when assigned roles.

### Change Notification Channel
Change the notification channel to which Cerberus sends information and error messages.

### Change Prefix
Change the prefix that Cerberus listens for

### Turn On / Off
Toggle Cerberus on / off. When Cerberus is off, it will no longer give new users roles.

### Check Status
Check the status of Cerberus to see whether it is currently on or off.

### Help Information
Get help information describing the commands and their usage.