require("dotenv").config();
const Discord = require("discord.js");

const { client } = require("./client");
const Error = require("./errors");

const { giveMemberNextRole } = require("./listeners/GuildMemberAddListener");
const { deleteRole } = require("./listeners/DeleteRoleListener");

client.on("roleDelete", deleteRole);

client.on("guildMemberAdd", (member) =>
  giveMemberNextRole(client, member, false)
);

client.on("commandError", async (command, err, message, args) => {
  const notificationChannel = await client.getNotificationsChannel(
    message.guild
  );
  if (err instanceof Error.InvalidArgumentError) {
    notificationChannel.send(
      new Discord.MessageEmbed()
        .setColor("#ff0000")
        .setTitle("Oops! :x:")
        .setDescription(err.message)
    );
  } else if (err instanceof Error.InvalidRoleError) {
    notificationChannel.send(
      new Discord.MessageEmbed()
        .setColor("#ff0000")
        .setTitle("Invalid AutoRole Configuration! :x:")
        .setDescription(err.message)
    );
  } else if (err instanceof Error.InvalidUsageError) {
    notificationChannel.send(
      new Discord.MessageEmbed()
        .setColor("#ff0000")
        .setTitle("Invalid Usage! :x:")
        .setDescription(err.message + `\n The accepted format is: ${err.usage}`)
    );
  } else if (err instanceof Error.InvalidPermissionsError) {
  } else {
    console.log("Command Error: ", err);
  }
});

client.once("ready", () => {
  console.log(`Logged in as ${client.user.username}`);
});

client.login(process.env.TOKEN);

process.on("uncaughtException", (error) => {
  console.log(error);
});
